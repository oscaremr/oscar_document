This is the general purpose documetation repository for oscar / myoscar and related projects.

The purpose of this repository is to hold documentation which pertains to multiple projects or general infrastructure documention.

Specific project documentation should still remain in the docs directory of each project.

Examples of shared documentation : 
1) a listing of all projects and brief description and location of them
2) infrastructure documentation like our git/gerrit and jenkins setup
3) third party software configuration information like a default my.cnf for mysql and what settings to use
4) cross project documentation like how the different library / utility projects relate to one another.

Documentation should be written in an editable format with free/open source editors, examples include : 
1) plain text
2) libreoffice
3) markdown
* notably not PDF as it's not readily editable, and not MS-Word as it's not free/opensource. 