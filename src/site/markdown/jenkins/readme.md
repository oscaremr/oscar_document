Jenkins
=======

URL: https://demo.oscarmcmaster.org:11042



Building for Gerrit Validation
------------------------------
When a patchset is submitted to Gerrit. Jenkins is called to build and verify the change.

If  *mvn clean verify* is successful the change will be marked as *Verified* on Gerrit.



Building for Master
-------------------
Preforms builds using the sourceforge git repository.  


Building for Maven Repo
-----------------------
This Job publishes the artifacts to sourceforge repo http://oscarmcmaster.sf.net/m2


Building for Selenium Oscar
---------------------------
Still in beta but nightly verifies that a series of tests can still be preformed in the browser.
