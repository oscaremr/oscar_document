Misc Info about java
=

Typical parameter
-
	MEM_SETTINGS=" -Xms384m -Xmx384m -Xss512k -XX:NewSize=128m -XX:MaxNewSize=128m -XX:MaxPermSize=128m -Xincgc -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70"
	export JAVA_OPTS=" -Djava.awt.headless=true -server -Xshare:off "${MEM_SETTINGS}
	

OpenJdk v.s. OracleJdk
-
We will only support oracle jdk. Our attempts to move to openjdk failed as open jdk does not provide binary extraction installs, we were able to easily extract the rpm packages but not the deb packages. This prevented us from simultaneously running multiple versions at the same time on the same machine.


Unsupported openJdk notes
-
Open jdk only comes in install-package form (rpm/deb, not bin) so here's how to manually extract packages so you can have multi-version installations.

	* rpm2cpio java-1.7.0-openjdk-1.7.0.9-2.3.5.fc16.x86_64.rpm  | cpio -ivd
	* rpm2cpio java-1.7.0-openjdk-devel-1.7.0.9-2.3.5.fc16.x86_64.rpm  | cpio -ivd
	
Be warned that this will create a slew of directories, best to make a temporary directory first and run the command from in there.

You can ignore everything except **usr/lib/jvm/java-1.7.0-openjdk-1.7.0.9.x86_64**, everything else is just man pages or sym links back into here.

You want to move/copy that directory to what you would normally bin-extract java to, i.e. /opt/myoscar_server2/openjdk1.7.0.9, set your JAVA_HOME and PATH and you should be ready to go.