| Name | Type | Maintainer/Origin | Code Location | WebSite | Description |
| - | - | - | - | - | - |
|Oscar|webapp|mcmaster|gerrit|sourceforge:oscarmcmaster|The main Oscar EMR|
|Integrator|webapp|caisi|gerrit|see oscar|A self standing server which oscar instances send data and retrive data from to allow sharing of data records between multiple oscar instances.|
|Drugref|webapp|mcmaster| googlecode ??? |???|A self standing webapp which reads drug data from blah blah ???, and makes the data available over web service calls|
|MyDrugRef | ??? | ??? | ??? | ??? | ??? |
|ar2005 | ??? | ??? |gerrit| ??? | ??? |
|ckd | ??? | ??? | gerrit | ??? | ??? |
|hl7_file_management | ??? | ??? | gerrit | ??? | CML and Lifelab interfaces ??? |
|hnr | webapp | caisi | gerrit | see oscar | Health Number Registry, simple webapp with web service calls to add a demographic with a health card number and web service calls to search for people or health numbers in the system. This allows multiple oscar/integrator instances to share demographics & health card numbers. |
|iha_auto_lab | ??? | ??? | gerrit | ??? | Interior Health Lab Interface ??? |
|myoscar_client2 | webapp | mcmaster | gerrit | sourceforge:myoscar | The webapp which provides patients with a GUI for the myoscar system, should be more appropriately named myoscar_patient_ui. This server communicates with the myoscar_server. |
|myoscar_client_server_commons | library | mcmaster | gerrit | see myoscar_client2 | java classes / constants / strings which are common to the myoscar_server and myoscar_clients |
|myoscar_client_utils | library | mcmaster | gerrit | see myoscar_client2 | utilitiy classes which maybe helpful for people writing clients that communicate with against the myoscar server. Examples include myoscar_client, oscar, and any myoscar server components. |
|myoscar_server2 | webapp | mcmaster | gerrit | see myoscar_client2 | MyOscar is a PHR system. This is the server which stores all the health records. Access to the server (read/write of health records) is via web services. There is no GUI on this server. |
|oscar_apps | webapp | mcmaster | gerrit | see oscar | This projects was originally intended to house multiple "apps" for oscar, but as of this writing the only "app" is a webapp which lets patients check-in upon arrival at a clinic by swiping their health card. This webapp contacts the oscar server and marks the patient / appointment as "arrived". |
|oscar_clinic_component | webapp | mcmaster | gerrit | ??? | This project is a set of web pages which allows patients to book and appointment online. The webpages are initiated from the myoscar_client (read as login to the myoscar client to see these web pages) but the appointment is booked in oscar (read as calls an oscar web service to set the patient in the providers schedule). |
|oscar_documents | webapp | mcmaster | gerrit | see oscar | ??? |
|oscar_maven_repo | maven repository | mcmaster | gerrit | see oscar | This is a maven repository which is referenced by all oscar and myoscar projects. The purpose of this repository is to allow developers to distribute changed jars to other people with out giving everyone access to an online website-maven-repo. |
|oscar_myoscar_data_sync | webapp | mcmaster | gerrit | see oscar | This is an oscar component and requires oscar. This component is meant to automatically push data from oscar to myoscar so that patients can see their medical records on myoscar. |
|oscar_plugins | library | mcmaster | gerrit | see oscar | This is a collection of maven plugins that we've custom written ourselves. These plugins are used in oscar and myoscar projects as per their respective pom.xml entries. |
|oscar_rope | ??? | caisi | gerrit | see oscar | ??? |
|oscar_ws_client_utils | library | mcmaster | gerrit | see oscar | This is a collection of convenience classes for clients of oscar. i.e. components of oscar. |
|selenium_oscar | test code | mcmaster | gerrit | see oscar | Selenium tests for oscar ??? |
|signature_pad | applet | caisi | gerrit | see oscar | This code produces a jar file. The jar file should then be copied into the oscar webapp (to existing file location). When a user is using oscar, in certain places there's options to use signature pads to sign, the jar is an applet that runs in the browser which then sends the image of the signature back to oscar. |
|utils | library | mcmaster | gerrit | see oscar | These are common general purpose utility classes used by virtually all oscar/myoscar related projects. |


	
