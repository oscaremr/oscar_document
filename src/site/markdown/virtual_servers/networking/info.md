What we want to provide here is 
* a sample configuration for the virtual network adapter that's known to work
* a test routing to verify that a given setup produces the network latency and through put desired

What we have here is 3 test files of varying size data
* a_few_bytes_test_text_file.txt
* 1k_test_image.jpg
* 1mb_test_image.jpg

The test assumes you have 1 physical computer, it's running a host operating system and a virtual instance where the virtual instance has a port openned externally (nat or it's own external IP or what ever).

It also assumes we'll run the test client on the same machine for simplicity. This should still be valid as we're only testing the difference between host and guest networking, not the actual network.

On both host and guess, an identical jdk & tomcat should be extracted somewhere temporary like /tmp or /home/user/test. The 3 test files should be copied into the root context of tomcat.

The test client should be running on the host system for testing both the host and guest system. The code is provided here. The code has no external dependencies so you can run it directly with the "java TestClient".

_

Here are sample results from my machine, i5-520m, 8gb ram, x86_64, oracle jdk1.7.0_17, tomcat 7.0.37

host : fedora 16
	http://192.168.1.5:8080/a_few_bytes_test_text_file.txt size=73
	total time=2984, avg=14.209524
	http://192.168.1.5:8080/1k_test_image.jpg size=1005
	total time=2570, avg=12.238095
	http://192.168.1.5:8080/1mb_test_image.jpg size=1151475
	total time=496635, avg=2364.9285
		
 
guest : 2gb ram, 4 cpus made available, fedora 16 x86_64
tested off the virtual bridge ip / unable to test through NAT as NAT only works for external requests.
	http://192.168.122.122:8080/a_few_bytes_test_text_file.txt size=73
	total time=5691, avg=27.1
	http://192.168.122.122:8080/1k_test_image.jpg size=1005
	total time=6355, avg=30.261906
	http://192.168.122.122:8080/1mb_test_image.jpg size=1151475
	total time=604153, avg=2876.919
		
			
The above seems to indicate a significant network performance difference.

There appears to be a significant overhead for connection establishment as indicated by the proportional time differences in small requests but not large requests.

The larger request appears to have roughly 20% impact which is still significant but in certain terms acceptable.
_

The Configuration 
-
guest network : source device "virtual network 'default' : NAT", device model "virtio"
host network : setup port-forwards in iptables/

The above configuration is assuming you only have 1 external IP. If you have multiple external IP's consider sharing the physical device.