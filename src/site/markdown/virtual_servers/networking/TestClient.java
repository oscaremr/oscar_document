import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class TestClient extends Thread
{
	private static final String SMALL_FILE = "a_few_bytes_test_text_file.txt";
	private static final String MEDIUM_FILE = "1k_test_image.jpg";
	private static final String LARGE_FILE = "1mb_test_image.jpg";

	private static int threadCount;
	private static int retrieveCount;

	public static void main(String... argv) throws Exception
	{
		if (argv.length != 3)
		{
			System.err.println("usage : TestClient <url_context> <threads_to_spawn> <retrieve_count>");
			System.err.println("example : TestClient http://127.0.0.1:8080 5 1000");
			System.err.println("will contact http://127.0.0.1:8080 and spawn 5 parallel request threads asking for all test files 1000 times");

			return;
		}

		threadCount = Integer.parseInt(argv[1]);
		retrieveCount = Integer.parseInt(argv[2]);
		runTests(argv[0]);
	}

	private static void runTests(String urlContext) throws IOException, InterruptedException
	{
		System.out.println("testing : url=" + urlContext + ", threadCount=" + threadCount + ", retrieveCount=" + retrieveCount);

		int temp = retrieveCount;
		retrieveCount = 100;
		System.out.println("discard this set, warm up");
		runSingleFileTest(SMALL_FILE, urlContext);
		runSingleFileTest(MEDIUM_FILE, urlContext);
		runSingleFileTest(LARGE_FILE, urlContext);

		retrieveCount = temp;
		System.out.println("this set should be fine");
		runSingleFileTest(SMALL_FILE, urlContext);
		runSingleFileTest(MEDIUM_FILE, urlContext);
		runSingleFileTest(LARGE_FILE, urlContext);
	}

	private static void runSingleFileTest(String file, String urlContext) throws IOException, InterruptedException
	{
		String urlPath = urlContext + '/' + file;

		// check connection and file size to make sure it's working.
		System.out.println(urlPath + " size=" + readAllBytes(urlPath));

		ArrayList<TestClient> threads = new ArrayList<TestClient>();
		for (int i = 0; i < threadCount; i++)
		{
			TestClient tc = new TestClient(urlPath);
			threads.add(tc);
			tc.start();
		}

		long totalTime = 0;
		for (TestClient tc : threads)
		{
			tc.join();
			totalTime = totalTime + tc.time;
		}
		System.out.println("total time=" + totalTime + ", avg=" + ((float) totalTime / (threadCount + retrieveCount)));
	}

	/**
	 * We want to force read all bytes for this test, not just read the declared data size.
	 */
	private static int readAllBytes(String urlPath) throws IOException
	{
		URL url = new URL(urlPath);
		URLConnection urlConnection = url.openConnection();
		InputStream is = urlConnection.getInputStream();
		try
		{
			int counter = 0;
			while (is.read() != -1)
			{
				counter++;
			}
			return(counter);
		}
		finally
		{
			is.close();
		}
	}

	private String urlPath;
	public long time;

	private TestClient(String urlPath)
	{
		this.urlPath = urlPath;
	}

	@Override
	public void run()
	{
		time = System.currentTimeMillis();
		try
		{
			for (int i = 0; i < retrieveCount; i++)
			{
				readAllBytes(urlPath);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		time = System.currentTimeMillis() - time;
	}
}
