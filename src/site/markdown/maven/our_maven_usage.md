This document outlines how we intend to use maven in the oscar projects. For the most part our usage of maven will be normal/standard.

oscar_maven_repo
-
Is our internal online maven repo. The repo holds all jars which we create and have source code to, i.e. utils. Jenkins should automatically publish the jars here. 


versioning
-
For library code we should verion changes down to the date i.e. 2013.03.04, for applications / war's it's sufficient to do a monthly snapshot (atleast for now).

When you change a version number (daily or monthly snapshot) you need to change it in multiple places. 
- It will be in the top of pom.xml as usual
- there maybe an entry in the plugins for deploying to the catalina_base
- there maybe an entry in mvn_install.sh for libraries
- there maybe a reference in jspc.xml for jsp compiles
- there maybe entries in build_client_stubs.sh for web service stub generations

One thing you can do is just 

   grep -r "2013\\.03\\.04" . 

to find all instances of that date.


code checking
-
By default most code validations should be on for the main build. i.e. junit tests, pmd checks, checkstyle checks, jsp compiles.

Developers can skip these tests during their regular development via "mvn -Dmaven.test.skip=true -Dcheckstyle.skip=true -Dpmd.skip=true -Dmaven.javadoc.skip=true package" which developers may wish to create and alias for. Having said that, developers should try to do the full build with complete checks before committing.


the utils project & dependencies
-
There's one anomaly in the utils project, most of the dependencies are declared as "provided" the reason for this is this is just a collection of random utilities, only the projects that use that utility really need the jar files at run time. As an example JsfUtils is utilities for jsf, by declaring the dependency as provided it means that if a project uses some other utility like DerbyUtils and doesn't use jsf at all, the project itself won't end up with jsf in it's set of libraries.  