This is the new documentation as of 2013-06-26, gerrit 2.6.1. The old document is starting to seriously fall out of date.

Basically this is a standard install so far.

We need to regularly run "ssh -p 29418 <your_user>@source.oscartools.org gerrit gc --all", don't run git gc anymore, us this command instead as of 2.6.1.


plugins / replication
-
As of gerrit 2.6.1 we need to install plugins for replication as it's no longer built in.

	At the time of this writing (2013-06-27) you have to compile binaries for plugins yourself. (a copy of the jar is in here)
	1) check out the replication source : git clone https://gerrit.googlesource.com/plugins/replication
	2) change the pom.xml : <Gerrit-ApiVersion>2.6</Gerrit-ApiVersion>
	3) mvn package 
	4) ssh -p 29418 <user>@source.oscartools.org gerrit plugin install -n replication - <replication-2.8-SNAPSHOT.jar (note for some reason the PATH version of this command wasn't working when I tried it, maybe because it expected a path on the gerrit machine)

a copy of the replication.config is here as well