Assumption is non developers are using windows.

-------------
Install a git
-------------
	- goto git-scm.com and get the windows client, install it.
	

-----------------
Generate SSH keys
-----------------
	This is basically the user/password system.
	(original reference : http://guides.beanstalkapp.com/version-control/git-on-windows.html#installing-ssh-keys)
	
	- goto the start menu, go to "git bash"
	- enter "ssh-keygen -t rsa" (and accept the defaults for all questions)
	- type "exit" and the window should close.
	

------------------
Sign up for gerrit
------------------
	(original reference : git_and_gerrit_info_for_developers.md)

	goto https://source.oscartools.org:8080
	
	On the gerrit main screen you should see a register option on the top right of the screen, you should be able to register using openId with your google email or one of the similar options.

	On the main registration screen :
	
	- For "Full Name" enter the persons name and click "save changed" (yes you actually need to click that now).
	- For "select a unique username" this will become your ssh access username, We'll use your email address (before the @ sign) as a standard. You need to click "select username" now.
	- For "Register an SSH public key"
		- open wordpad or notepad or some text editor
		- open the file in : "desktop" -> your home folder -> ".ssh" folder -> id_rsa.pub (Note you may have to select "all documents" before it shows up on the list, note you may see 2 files called "id_rsa", look under the "type" column for "pub file" to know which is the right one)
		- The contents of the file will look like jibberish similar to :
		
				ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAyyA8wePstPC69PeuHFtOwyTecByonsHFAjHbVnZ+h0dpomvLZxUtbknNj3+
				c7MPYKqKBOx9gUKV/diR/mIDqsb405MlrI1kmNR9zbFGYAAwIH/Gxt0Lv5ffwaqsz7cECHBbMojQGEz3IH3twEvDfF6cu5p
				00QfP0MSmEi/eB+W+h30NGdqLJCziLDlp409jAfXbQm/4Yx7apLvEmkaYSrb5f/pfvYv1FEV1tS8/J7DgdHUAWo6gyGUUSZ
				JgsyHcuJT7v9Tf0xwiFWOWL9WsWXa9fCKqTeYnYJhHlqfinZRnT/+jkz0OZ7YmXo6j4Hyms3RCOqenIX1W6gnIn+eQIkw==
				Mac Pro
				
		- copy and paste those contents exactly as is into the text box for the "add ssh public key" then click the "add button"
		- you can close the text editor now
		
	At this point you will have an account but to prevent spam/bots we have to manually enable write access to each account. You will need to email oscarmcmaster-devel@lists.sourceforge.net so we know you've signed up for an account so we can give you write access.
  

-------------------
Install TortoiseGit
-------------------
	- goto http://code.google.com/p/tortoisegit
	- download and install tortoise git


----------------------
Checking out a project
----------------------
	- you will need a working directory for all projects, in this example we'll use c:\Users\my_windows_user_name\git_root
	- make yourself a working directory if you don't have one.
	- open windows explorer and go into the git_root directory
	- right click in the explorer while in the git_root folder, "TortoiseGit" -> "Settings"
		- click on "git"
			- User Info : Name = your gerrit username
			- User Info : Email = your email used for gerrit
			- click ok to close that dialog
	- right click on the explorer while in the git_root folder again, "Git Clone"
		- url = ssh://your_gerrit_username@source.oscartools.org:29418/test
		- directory = c:\Users\my_windows_user_name\git_root\test
		- click ok, and it should start cloning the project to your HD.
		- It should eventually say "success", close the dialog.
		
	- You should now see a folder called "test", that folder now contains all the files for that project.
  
		
--------------
Making changes
--------------	
	- in windows explorer, go into c:\Users\my_windows_user_name\git_root\test
	- open one of the text files, add a few lines and save the file
	- create a new text file and add a few lines to the new file
	
	- now in windows explorer go back to c:\Users\my_windows_user_name\git_root
		- right click on the "test" folder, "Git commit -> master"
			- type in something in the message like "my test commit" 
			- notice in the bottom box it shows you a list of files
				- "modified files" contains the file you edited
				- "not versioned files" contains the new file you created, check the check box beside the new file.
				- at this point all files should be checkbox-checked, all checked files will be part of this commit. Click ok.
				- A dialog should show "success", there should be a "push" button, click the push button.
				- on the new dialog, change : Remote = "refs/for/master", click "ok".
				- There should eventually be a "success" dialog. Close the dialog.
				
				Making a "commit" just means which files you want bundled together.
				Making a "push" means send that bundle of files to the server.
				
				To verify things worked, you can go to https://source.oscartools.org:8080
				a new line item should appear for your commit. If you click on the commit
				you should see a list of files you sent in that commit and the changes in each file.
				

---------------
Getting changes
---------------
	Before you make any changes you should always get everyone elses changes. i.e.
	if 5 people are all editing the same file, you should get the other peoples changes
	before adding yours.
	
	- in windows explorer, goto c:\Users\my_windows_user_name\git_root
	- right click on "test", "git sync"
		- on the dialog click "pull" (this will retrieve the changes to your computer) 
			
		
