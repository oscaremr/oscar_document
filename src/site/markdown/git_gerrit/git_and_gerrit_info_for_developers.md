Git/Gerrit For Developers
=========================

This information is up to date as of 2013-04-24

This is brief information on our usage of git & gerrit. For basic information on git & gerrit please do a web search as there is tons of documentation out there on git now. One such example is http://wiki.eclipse.org/EGit/User_Guide/Remote

Oscar and it's related projects use Gerrit as our git server. Gerrit is a review system for git and includes a standard git repository. When we refer to gerrit urls and ports they can be use interchangably with git urls & ports so on your git client use that info.


server address
--------------
web : https://source.oscartools.org:8080
ssh : source.oscartools.org:29418


This document is very brief, you should have a basic understanding of GIT before doing this. This basically just explains the tools setup to use GIT, not what GIT is. You need to understand the concept of remote and local repositories.


Register a user on gerrit
-------------------------
Go to the gerrit web site and register your user.

On the gerrit main screen you should see a register option on the top right of the screen, you should be able to register using openId with your google email or one of the similar options.

On the main registration screen :

- For "Full Name" enter the persons name and click "save changed" (yes you actually need to click that now).
- For "select a unique username" this will become your ssh access username, We'll use your email address (before the @ sign) as a standard. You need to click "select username" now.
- For "Register an SSH public key" : if you already know or have SSH keys you can just provide it, otherwise do the following : 
	- on your client machine / developer machine : "ssh-keygen -t rsa", do not enter a password when it asks. This means... if you machine is compromised, your gerrit user is compromised and the corresponding server key should be deleted, you'll need to tell the gerrit administrator and or login to the gerrit web site and delete this key.
	- now cut and paste the contents of "~/.ssh/id_rsa.pub" into the text area on the web page.
	- click add and it should list it as an added key on the screen.
	- now on the bottom of the screen there's a link that says "continue" click that, now you should be at the your "code review dashboard" screen.
	
At this point you will have an account but to prevent spam/bots we have to manually enable write access to each account. You will need to email oscarmcmaster-devel@lists.sourceforge.net so we know you've signed up for an account so we can give you write access. You may wish to say hi to everyone and give a brief into into who you are too so we all have a better idea as to who everyone else is. :)

On the top right of the gerrit web pages there's a "Settings" option. In settings, the last menu will have a "watched projects" option. This will allow you to receive emails when ever anyone commits to the selected projects. This is also a convenient (and possibly the only) place to see a list of available projects on the system.

At this point, if your eclipse was already running, you need to stop it and start it again. Eclipse caches ssh key information. (really, it does)


get a git client
----------------
You're mostly on your own here, either command line git or egit for eclipse (http://www.eclipse.org/egit/) or what ever you want.



~/.gitconfig (required, even if using git from command line)
------------------------------------------------------------
You need a file in your home directory calls .gitconfig, in that file you need at least the [user] section, below is my .gitconfig for reference the email address should match the one you used for registering with gerrit.

	[user]
   		  	name = your name
       		email = your_email@example.com
	[core]
   		  	editor = nano

As a note, you can also edit this file in your eclipse->preferences->team->git, it opens the file in an editor panel for you.

You must do this before making any changes and local commits or the commit will be tagged to the wrong user.


check the connection works
--------------------------
Clone our "test" repository, you can try committing there too to make sure everything is working.

command line : 

	git clone ssh://<USER_NAME>@source.oscartools.org:29418/test 

eclipse : follow the egit instructions on how to clone / add a repository like at http://wiki.eclipse.org/EGit/User_Guide/Remote.

As a note, the push command is slightly different than standard git. The push specification is as follows : 

- source ref="HEAD"
- destination ref="refs/for/master"

If you don't change the push destination you'll receive and error when you try to push.



ChangeId Hooks 
--------------
Change ID Hook will add a change ID to every commit you make. The benefit of this is if you change / fix a commit and resubmit it, gerrit will use the changeId to replace the previous commit. 

Note that this conceptually the same as commit --amend but it's actually different because of the gerrit workflow. In general once you have a changeId in the commit message you will be able to use commit --amend on remote commits properly. If you don't have a changeId, a commit --amend will generate 2 commits (if the previous was already pushed) instead of replacing the previous one. 

command line :  To install the changeId hook for git command line usage, copy the commit-msg file (located in this directory) into your test/.git/hooks directory (you may need to make the hooks directory) this will effectively enable the changeId hook for the test project. When you commit it should add a changeId to the comments. You may need to make sure the file is chmod +x as it's a script.

eclipse : For Eclipse, the newer versions of the git plugin has a check box option on the commit screen to auto generate changeId's, you don't have to do anything except click that check box when you make a commit.  

       
Misc Usage Info
---------------
Some people may opt to use gerrit as a temporary working space, ie. commits which aren't meant to be submitted yet but more for sharing between other developers for review etc. These commits should clearly be marked "DO NOT REVIEW" in the first line of the commit message.

Please make sure you include gerrit changeId's with all commits. 